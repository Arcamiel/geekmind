from django.contrib import admin

from blog.models import Post
from blog.models import Comment
from blog.models import Category
from blog.models import like
from blog.models import dislike
from blog.models import Termos_de_Uso

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(like)
admin.site.register(dislike)
admin.site.register(Termos_de_Uso)