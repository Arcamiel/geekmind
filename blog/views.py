from django.shortcuts import render
from django.db.models import Q
from django.views.generic import ListView, DetailView
from blog.models import Post, Category, Comment, Termos_de_Uso
from django.core.paginator import Paginator


class SearchView(ListView):
    model = Post
    template_name = 'search.html'

    def get_queryset(self):
        query = self.request.GET.get('Pes')
        object_list = Post.objects.filter(
            Q(title__icontains=query) | 
            Q(text__icontains=query) |
            Q(category__name__icontains=query)
        )
        return object_list



class LayoutView:
    
    def get_context_data(self, **kwargs):
      context = super().get_context_data(**kwargs)
      context['categories'] = category.objects.all()
      return context

      
class PostListView(ListView, LayoutView):
    model = Post

class PackageCustomList(ListView, LayoutView):
  model = Post
  template_name = 'post/custom_list.html'
  paginate_by = 3
 
  def get_queryset(self, *args, **kwargs):
    if self.kwargs:
      return Post.objects.filter(category=self.kwargs['category']).order_by('-createdAt')
    else:
      query = Post.objects.all().order_by('-createdAt')
      return query

class PostAboutListView(ListView, LayoutView):
    model = Post

class PostPerfilListView(ListView, LayoutView):
    model = Post

class CategoryDetailView(DetailView, LayoutView):
    model = Category

class PostDetailView(DetailView, LayoutView):
    model = Post

class TduDetailView(LayoutView, DetailView):
    model = Termos_de_Uso